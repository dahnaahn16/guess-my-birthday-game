from random import randint
name = input("hi! what is your name? ")

for guess_number in range(1,6):
    month_number = randint(1,12)
    year_number = randint(1924, 2004)

    print("guess", guess_number, ":were you born in ", month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("i have other things to do. Goodbye.")
    else:
        print("Drat. let me try again.")
